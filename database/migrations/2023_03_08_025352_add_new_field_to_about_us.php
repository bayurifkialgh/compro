<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('about_us', function (Blueprint $table) {
            $table->string('logo')->nullable()->after('name');
            $table->json('history')->nullable()->after('slogan');
            $table->text('vision')->nullable()->after('slogan');
            $table->text('mision')->nullable()->after('slogan');
            $table->text('address')->nullable()->after('slogan');
            $table->string('phone', 15)->nullable()->after('slogan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('about_us', function (Blueprint $table) {
            //
        });
    }
};
