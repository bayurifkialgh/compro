<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Configuration;

class ConfigurationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Web meta
        $metas = [
            'title' => 'Laravel 8',
            'author' => 'Laravel',
            'description' => 'Laravel 8',
            'keywords' => 'Laravel 8',
            'og:type' => 'website',
            'og:title' => 'Laravel 8',
            'og:description' => 'Laravel 8',
            'og:image' => 'https://laravel.com/img/logomark.min.svg',
            'og:url' => 'https://laravel.com/',
            'twitter:card' => 'summary',
            'twitter:title' => 'Laravel 8',
            'twitter:description' => 'Laravel 8',
            'twitter:image' => 'https://laravel.com/img/logomark.min.svg',
            'twitter:url' => 'https://laravel.com/',
        ];

        $metas = json_encode($metas);

        // Web setting
        $websites = [
            'app_name' => 'Laravel 8',
            'app_icon' => 'assets/img/favicon/favicon.ico',
            'app_apple_icon' => 'assets/img/favicon/apple-touch-icon.png',
            'icon_32x32' => 'assets/img/favicon/favicon-32x32.png',
            'icon_16x16' => 'assets/img/favicon/favicon-16x16.png',
            'manifest' => 'assets/img/favicon/site.webmanifest',

        ];

        $websites = json_encode($websites);

        // Web social
        $socials = [
            'facebook' => 'https://www.facebook.com/',
            'twitter' => 'https://twitter.com/',
            'instagram' => 'https://www.instagram.com/',
            'youtube' => 'https://www.youtube.com/',
            'linkedin' => 'https://www.linkedin.com/',
            'github' => 'https://www.linkedin.com/',
        ];

        $socials = json_encode($socials);

        $term_condition = `# Term & Condition

        Welcome to Karuhun Creative

        These terms and conditions outline the rules and regulations for the use of Karuhun Creative's Website, located at url.

        By accessing this website we assume you accept these terms and conditions. Do not continue to use Karuhun Creative if you do not agree to take all of the terms and conditions stated on this page.

        The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and all Agreements: "Client", "You" and "Your" refers to you, the person log on this website and compliant to the Company’s terms and conditions. "The Company", "Ourselves", "We", "Our" and "Us", refers to our Company. "Party", "Parties", or "Us", refers to both the Client and ourselves. All terms refer to the offer, acceptance and consideration of payment necessary to undertake the process of our assistance to the Client in the most appropriate manner for the express purpose of meeting the Client’s needs in respect of provision of the Company’s stated services, in accordance with and subject to, prevailing law of Netherlands. Any use of the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are taken as interchangeable and therefore as referring to same.`;

        Configuration::create([
            'metas' => $metas,
            'website' => $websites,
            'socials' => $socials,
            'term_condition' => $term_condition,
            'privacy_policy' => '# Privacy Policy',
        ]);
    }
}
