<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\TechExpertise;

class TechSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $service = [
            'Web Development' => '1',
            'Mobile App Development' => '2',
            'Game Development' => '3',
            'SEO Website' => '4',
        ];

        $data = [
            [
                'service_id' => $service['Web Development'],
                'name' => 'Javascript',
                'icon' => 'fab fa-js',
                'slug' => Str::slug('Javascript'),
                'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam, quod.',
            ],
            [
                'service_id' => $service['Web Development'],
                'name' => 'PHP',
                'icon' => 'fab fa-php',
                'slug' => Str::slug('PHP'),
                'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam, quod.',
            ],
            [
                'service_id' => $service['Game Development'],
                'name' => 'Java',
                'icon' => 'fab fa-java',
                'slug' => Str::slug('Java'),
                'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam, quod.',
            ],
            [
                'service_id' => $service['Web Development'],
                'name' => 'Laravel',
                'icon' => 'fab fa-laravel',
                'slug' => Str::slug('Laravel'),
                'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam, quod.',
            ],
            [
                'name' => 'Vue.js',
                'icon' => 'fab fa-vuejs',
                'slug' => Str::slug('Vue.js'),
                'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam, quod.',
            ],
            [
                'service_id' => $service['Web Development'],
                'name' => 'React.js',
                'icon' => 'fab fa-react',
                'slug' => Str::slug('React.js'),
                'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam, quod.',
            ],
            [
                'service_id' => $service['Web Development'],
                'name' => 'Bootstrap',
                'icon' => 'fab fa-bootstrap',
                'slug' => Str::slug('Bootstrap'),
                'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam, quod.',
            ],
            [
                'service_id' => $service['Web Development'],
                'name' => 'Tailwind CSS',
                'icon' => 'fab fa-css3-alt',
                'slug' => Str::slug('Tailwind CSS'),
                'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam, quod.',
            ]
        ];

        TechExpertise::insert($data);
    }
}
