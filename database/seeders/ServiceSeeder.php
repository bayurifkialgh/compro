<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\Service;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Web Development',
                'icon' => 'fas fa-pager',
                'slug' => Str::slug('Web Development'),
                'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam, quod.',
            ],
            [
                'name' => 'Mobile App Development',
                'icon' => 'fas fa-code',
                'slug' => Str::slug('Mobile App Development'),
                'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam, quod.',
            ],
            [
                'name' => 'Game Development',
                'icon' => 'fas fa-gamepad',
                'slug' => Str::slug('Game Development'),
                'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam, quod.',
            ],
            [
                'name' => 'SEO Website',
                'icon' => 'fas fa-search',
                'slug' => Str::slug('SEO Website'),
                'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam, quod.',
            ],
        ];

        Service::insert($data);
    }
}
