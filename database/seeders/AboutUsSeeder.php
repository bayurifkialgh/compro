<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\AboutUs;

class AboutUsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'name' => 'Karuhun Creative',
            'slogan' => 'Creative & Innovative',
            'phone' => '08123123',
            'address' => 'Cimahi, Jawa Barat',
            'mision' => 'Misson',
            'vision' => 'Vision',
            'description' => 'IncludeStudio merupakan jasa pembuatan dan pengembangan IT yang sudah berdiri sejak 2018 dengan pengalaman membuat kebutuhan IT untuk beberapa lembaga dan UMKM. Kami bersedia membantu bisnis anda dengan layanan yang kami sediakan tentunya dengan biaya yang terjangkau. Berikut adalah layanan IT yang kami sediakan'
        ];

        AboutUs::create($data);
    }
}
