<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    use HasFactory;

    protected $fillable = [
        'service_id',
        'client_id',
        'name',
        'image',
        'url',
        'is_blank',
        'description',
        'year',
    ];
}
