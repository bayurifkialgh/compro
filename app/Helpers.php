<?php

namespace App;

class Helpers {

    public static function uploadImage($file, $path = 'file', $base_file_name = 'file')
    {
        // Set base file name
        $base_file_name = $base_file_name . '_' . date('d-m-Y') . '_';

        // If file is not exist
        if(!$file) return false;

        $filename = $base_file_name . uniqid() . '.' . $file->extension();
        $file->storeAs('public/' . $path, $filename);

        return [
            'filename' => $filename,
            'path' => 'public/' . $path,
        ];
    }
}
