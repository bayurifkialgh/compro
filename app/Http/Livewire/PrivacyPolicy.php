<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Configuration;

class PrivacyPolicy extends Component
{
    public $title = 'Privacy Policy';

    public $privacy_policy;

    public function mount() {
        $this->privacy_policy = Configuration::first()->privacy_policy;
    }

    public function render() {
        return view('livewire.privacy-policy')->layout(\App\View\Components\AppLayout::class);;
    }
}
