<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Service;
use App\Models\ContactUs as ContactUsModel;

class ContactUs extends Component
{
    public $title = 'Contact Us';

    public $data, $services;

    protected $rules = [
        'data.name' => 'required',
        'data.company' => 'required',
        'data.email' => 'required',
        'data.contact' => 'required',
        'data.service_id' => 'required',
        'data.project' => 'required',
        'data.message' => 'required',
    ];

    protected $messages = [
        'data.name.required' => 'Name is required.',
        'data.company.required' => 'Company is required.',
        'data.email.required' => 'Email is required.',
        'data.contact.required' => 'Contact is required.',
        'data.service_id.required' => 'Service is required.',
        'data.project.required' => 'Project is required.',
        'data.message.required' => 'Message is required.',
    ];

    public function mount() {
        $this->services = Service::all();
    }

    public function render() {
        return view('livewire.contact-us')->layout(\App\View\Components\AppLayout::class);
    }

    public function save() {
        $this->validate();

        if(session('contact_us') > now()) {
            $this->emit('alert', 'Please wait for 5 minutes before sending another message.', 'warning');
            return;
        }

        // Create message
        $exe = ContactUsModel::create($this->data);

        // Five minutes wait time
        session()->put('contact_us', now()->addMinutes(5));

        $this->emit('alert', 'Message sent successfully.');
    }
}
