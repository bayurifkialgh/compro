<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\AboutUs as AboutUsModel;

class AboutUs extends Component
{
    public $title = 'About Us';

    public $aboutUs;

    public function mount() {
        $this->aboutUs = AboutUsModel::first();
    }

    public function render()
    {
        return view('livewire.about-us')->layout(\App\View\Components\AppLayout::class);
    }
}
