<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Team as TeamModel;

class Team extends Component
{
    public $title = 'Team';

    public $teams;

    public function mount() {
        $this->teams = TeamModel::all();
    }

    public function render()
    {
        return view('livewire.team')->layout(\App\View\Components\AppLayout::class);
    }
}
