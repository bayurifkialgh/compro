<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Portfolio as PortfolioModel;

class Portfolio extends Component
{
    public $title = 'Portfolio';

    public $portfolios;

    public function mount() {
        $this->portfolios = PortfolioModel::all();
    }

    public function render()
    {
        return view('livewire.portfolio')->layout(\App\View\Components\AppLayout::class);
    }
}
