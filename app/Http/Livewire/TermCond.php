<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Configuration;

class TermCond extends Component
{
    public $title = 'Term & Condition';

    public $term_condition;

    public function mount() {
        $this->term_condition = Configuration::first()->term_condition;
    }

    public function render() {
        return view('livewire.term-cond')->layout(\App\View\Components\AppLayout::class);;
    }
}
