<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\ContactUs as ContactUsModel;
use Livewire\WithPagination;

class ContactUs extends Component
{
    use WithPagination;

    public $title = 'Contact Us';
    public $searchable = [
        'services.name',
        'contact_us.name',
        'contact_us.company',
        'contact_us.email',
        'contact_us.contact',
        'contact_us.project',
        'contact_us.message',
    ];
    public $search = '',
        $paginate = 10,
        $orderBy = 'contact_us.created_at',
        $order = 'asc',
        $isUpdate = false;

    public function render()
    {
        $sql = ContactUsModel::leftJoin('services', 'services.id', '=', 'contact_us.service_id')
                ->orderBy($this->orderBy, $this->order)
                ->select('contact_us.*', 'services.name as service')
                ->latest();
        $get = $sql->paginate($this->paginate);

        // Search data
        if ($this->search != null) {
            $get = $sql;

            foreach ($this->searchable as $field) {
                $get = $get->orWhere($field, 'like', "%{$this->search}%");
            }

            $get = $get->paginate($this->paginate);

            $this->resetPage();
        }

        return view('livewire.admin.contact-us', compact('get'));
    }

    // Order by
    public function changeOrder($orderBy)
    {
        if ($this->orderBy == $orderBy) {
            $this->order = $this->order == 'desc' ? 'asc' : 'desc';
        }

        $this->orderBy = $orderBy;
    }
}
