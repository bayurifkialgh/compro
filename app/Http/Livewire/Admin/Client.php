<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\Client as ClientModel;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Helpers;

class Client extends Component
{
    use WithPagination, WithFileUploads;

    public $title = 'Clients';
    public $searchable = ['name', 'url'];
    public $search = '',
        $uid,
        $data,
        $paginate = 10,
        $orderBy = 'name',
        $order = 'asc',
        $isUpdate = false;

    protected $listeners = [
        'delete' => 'destroy',
        'changeOrder',
    ];

    protected $rules = [
        'data.name' => 'required',
        'data.image' => 'nullable|mimes:jpeg,bmp,png,svg,webp,jpg,ico',
        'data.url' => 'required',
    ];

    protected $messages = [
        'data.name.required' => 'Name is required.',
        'data.image.mimes' => 'Image must be a file of type: jpeg, bmp, png, svg, webp, jpg, ico.',
        'data.url.required' => 'URL is required.',
    ];

    public function render()
    {
        $sql = ClientModel::orderBy($this->orderBy, $this->order)->latest();
        $get = $sql->paginate($this->paginate);

        // Search data
        if ($this->search != null) {
            $get = $sql;

            foreach ($this->searchable as $field) {
                $get = $get->orWhere($field, 'like', "%{$this->search}%");
            }

            $get = $get->paginate($this->paginate);

            $this->resetPage();
        }
        return view('livewire.admin.client', compact('get'));
    }

    // Order by
    public function changeOrder($orderBy)
    {
        if ($this->orderBy == $orderBy) {
            $this->order = $this->order == 'desc' ? 'asc' : 'desc';
        }

        $this->orderBy = $orderBy;
    }

    // Confirm delete
    public function confirmDelete($id) {
        $this->emit('confirm', $id);
    }

    // Delete data
    public function destroy($id) {
        $exe = ClientModel::find($id);
        $exe->delete();

        $this->emit('alert', 'Delete data success');
    }

    // Get detail data
    public function getDetail($id) {
        // Get
        $get = ClientModel::find($id)->makeHidden([
            'created_at', 'updated_at', 'image'
        ])->toArray();

        // Set is update
        $this->isUpdate = true;

        // Set id
        $this->uid = $get['id'];

        unset($get['id']);
        $get['image'] = '';

        $this->data = $get;
    }

    // Save data
    public function save() {
        $this->validate();
        $message = 'data success';

        // Insert
        if (!$this->isUpdate) {
            // Upload image
            if($this->data['image']) {
                $upload = Helpers::uploadImage($this->data['image'], 'client', 'client');
                $this->data['image'] = $upload['filename'];
            }

            $exe = ClientModel::create($this->data);
            $message = 'Create ' . $message;
        }

        // Update
        else {
            $exe = ClientModel::find($this->uid);

            // Upload image
            if($this->data['image']) {
                $upload = Helpers::uploadImage($this->data['image'], 'client', 'client');
                $this->data['image'] = $upload['filename'];
            } else {
                $this->data['image'] = $exe['image'];
            }

            $exe->update($this->data);

            $message = 'Update ' . $message;
        }

        // Clear form
        $this->clearForm();

        // Emit alert
        $this->emit('alert', $message);
        $this->emit('closeModal');
    }

    // Clear form
    public function clearForm() {
        // Reset is update
        $this->isUpdate = false;
        $this->uid = '';
        $this->data = [
            'name' => '',
            'image' => '',
            'url' => '',
        ];
    }
}
