<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\AboutUs as AboutUsModel;
use Livewire\WithFileUploads;
use Helpers;

class AboutUs extends Component
{
    use WithFileUploads;

    public $title = 'About Us';
    public $data;
    public $logo;

    protected $rules = [
        'data.name' => 'required',
        'data.slogan' => 'required',
        'data.phone' => 'required',
        'data.description' => 'required',
        'data.logo' => 'nullable|mimes:jpeg,bmp,png,svg,webp,jpg,ico',
    ];

    protected $messages = [
        'data.name.required' => 'Name is required.',
        'data.slogan.required' => 'Slogan is required.',
        'data.phone.required' => 'Phone is required.',
        'data.description.required' => 'Description is required.',
        'data.logo.mimes' => 'Logo must be a file of type: jpeg, bmp, png, svg, webp, jpg, ico.',
    ];

    public function mount() {
        $get = AboutUsModel::first()->makeHidden([
            'created_at', 'updated_at'
        ])->toArray();

        $this->logo = $get['logo'];
        $this->data = $get;
        $this->data['logo'] = '';
    }

    public function render()
    {
        return view('livewire.admin.about-us');
    }

    public function save() {
        // Scroll to top
        $this->emit('scrollUp');

        $this->validate();

        // Upload logo
        if($this->data['logo']) {
            $upload = Helpers::uploadImage($this->data['logo'], 'about-us', 'about-us');
            $this->data['logo'] = $upload['filename'];
            $this->logo = $upload['filename'];
        } else {
            $this->data['logo'] = $this->logo;
        }

        $exe = AboutUsModel::first()->update($this->data);

        $this->data['logo'] = '';
        $this->emit('alert', 'About us data has been updated.');
    }
}
