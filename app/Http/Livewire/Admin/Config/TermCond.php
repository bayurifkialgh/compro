<?php

namespace App\Http\Livewire\Admin\Config;

use Livewire\Component;
use App\Models\Configuration;

class TermCond extends Component
{
    public $title = 'Term & Condition';

    public function mount() {
        $get = Configuration::first();
        $this->term_condition = $get->term_condition;
    }

    public function render() {
        return view('livewire.admin.config.term-cond');
    }

    public function save() {
        $exe = Configuration::first()->update([
            'term_condition' => $this->term_condition,
        ]);

        $this->emit('alert', 'Term & condition has been updated.');
    }
}
