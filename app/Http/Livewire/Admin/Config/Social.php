<?php

namespace App\Http\Livewire\Admin\Config;

use Livewire\Component;
use App\Models\Configuration;

class Social extends Component
{
    public $title = 'Social Configuration';
    public $data;

    protected $rules = [
        'data.github' => 'required',
        'data.twitter' => 'required',
        'data.whatsapp' => 'required',
        'data.phone' => 'required',
        'data.youtube' => 'required',
        'data.facebook' => 'required',
        'data.linkedin' => 'required',
        'data.instagram' => 'required',
    ];

    protected $messages = [
        'data.github.required' => 'Github is required',
        'data.twitter.required' => 'Twitter is required',
        'data.whatsapp.required' => 'Whatsapp is required',
        'data.phone.required' => 'Phone is required',
        'data.youtube.required' => 'Youtube is required',
        'data.facebook.required' => 'Facebook is required',
        'data.linkedin.required' => 'Linkedin is required',
        'data.instagram.required' => 'Instagram is required',
    ];

    public function mount() {
        $get = Configuration::first();
        $this->data = json_decode($get->socials, true);
    }

    public function render() {
        return view('livewire.admin.config.social');
    }

    public function save() {
        $this->validate();

        $data = json_encode($this->data);

        $exe = Configuration::first()->update([
            'socials' => $data,
        ]);

        $this->emit('alert', 'Social data has been updated.');
    }
}
