<?php

namespace App\Http\Livewire\Admin\Config;

use Livewire\Component;
use App\Models\Configuration;

class Meta extends Component
{
    public $title = 'Meta Configuration';
    public $data;

    protected $rules = [
        'data.title' => 'required',
        'data.author' => 'required',
        'data.keywords' => 'required',
        'data.description' => 'required',
        'data.og:type' => 'required',
        'data.og:title' => 'required',
        'data.og:image' => 'required',
        'data.og:description' => 'required',
        'data.twitter:card' => 'required',
        'data.twitter:image' => 'required',
        'data.twitter:title' => 'required',
        'data.twitter:description' => 'required',
    ];

    protected $messages = [
        'data.title.required' => 'Title is required.',
        'data.author.required' => 'Author is required.',
        'data.keywords.required' => 'Keywords is required.',
        'data.description.required' => 'Description is required.',
        'data.og:type.required' => 'Open Graph type is required.',
        'data.og:title.required' => 'Open Graph title is required.',
        'data.og:image.required' => 'Open Graph image url is required.',
        'data.og:description.required' => 'Open Graph description is required.',
        'data.twitter:card.required' => 'Twitter card is required.',
        'data.twitter:image.required' => 'Twitter image url is required.',
        'data.twitter:title.required' => 'Twitter title is required.',
        'data.twitter:description.required' => 'Twitter description is required.',
    ];

    public function mount() {
        $get = Configuration::first();
        $this->data = json_decode($get->metas, true);
    }

    public function render() {
        return view('livewire.admin.config.meta');
    }

    public function save() {
        $this->validate();

        $data = json_encode($this->data);

        $exe = Configuration::first()->update([
            'metas' => $data,
        ]);

        $this->emit('alert', 'Meta data has been updated.');
    }
}
