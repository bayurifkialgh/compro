<?php

namespace App\Http\Livewire\Admin\Config;

use Livewire\Component;
use App\Models\Configuration;
use Livewire\WithFileUploads;
use Helpers;

class Website extends Component
{
    use WithFileUploads;

    public $title = 'Website Configuration';
    public $website,
        $app_name,
        $app_icon,
        $icon_16x16,
        $icon_32x32,
        $app_apple_icon,
        $manifest;

    protected $rules = [
        'app_name' => 'required',
        'app_icon' => 'nullable|mimes:jpeg,bmp,png,svg,webp,jpg,ico',
        'icon_16x16' => 'nullable|mimes:jpeg,bmp,png,svg,webp,jpg,ico',
        'icon_32x32' => 'nullable|mimes:jpeg,bmp,png,svg,webp,jpg,ico',
        'app_apple_icon' => 'nullable|mimes:jpeg,bmp,png,svg,webp,jpg,ico',
    ];

    protected $messages = [
        'app_name.required' => 'App name is required.',
        'app_icon.mimes' => 'App icon must be a file of type: jpeg, bmp, png, svg, webp, jpg, ico.',
        'icon_16x16.mimes' => 'Icon 16x16 must be a file of type: jpeg, bmp, png, svg, webp, jpg, ico.',
        'icon_32x32.mimes' => 'Icon 32x32 must be a file of type: jpeg, bmp, png, svg, webp, jpg, ico.',
        'app_apple_icon.mimes' => 'App apple icon must be a file of type: jpeg, bmp, png, svg, webp, jpg, ico.',
    ];

    public function mount() {
        $get = Configuration::first();
        $this->website = json_decode($get->website, true);

        // Data
        $this->app_name = $this->website['app_name'];
    }

    public function render() {
        return view('livewire.admin.config.website');
    }

    // Save
    public function save() {
        $this->validate();
        $this->upload();

        $data = json_encode([
            'app_name' => $this->app_name,
            'app_icon' => $this->app_icon,
            'icon_16x16' => $this->icon_16x16,
            'icon_32x32' => $this->icon_32x32,
            'app_apple_icon' => $this->app_apple_icon,
            'manifest' => $this->manifest,
        ]);

        $exe = Configuration::first()->update([
            'website' => $data,
        ]);

        // Data
        $get = Configuration::first();
        $this->website = json_decode($get->website, true);
        $this->resetExcept(['app_name', 'website']);
        $this->emit('alert', 'Website data has been updated.');
    }

    // Upload file
    public function upload() {
        // App icon
        if($this->app_icon) {
            $upload = Helpers::uploadImage($this->app_icon, 'websites', 'websites');
            $this->app_icon = $upload['filename'];
        } else {
            $this->app_icon = $this->website['app_icon'];
        }

        // Icon 16x16
        if($this->icon_16x16) {
            $upload = Helpers::uploadImage($this->icon_16x16, 'websites', 'websites');
            $this->icon_16x16 = $upload['filename'];
        } else {
            $this->icon_16x16 = $this->website['icon_16x16'];
        }

        // Icon 32x32
        if($this->icon_32x32) {
            $upload = Helpers::uploadImage($this->icon_32x32, 'websites', 'websites');
            $this->icon_32x32 = $upload['filename'];
        } else {
            $this->icon_32x32 = $this->website['icon_32x32'];
        }

        // App apple icon
        if($this->app_apple_icon) {
            $upload = Helpers::uploadImage($this->app_apple_icon, 'websites', 'websites');
            $this->app_apple_icon = $upload['filename'];
        } else {
            $this->app_apple_icon = $this->website['app_apple_icon'];
        }

        // Manifest
        if($this->manifest) {
            $upload = Helpers::uploadImage($this->manifest, 'websites', 'websites');
            $this->manifest = $upload['filename'];
        } else {
            $this->manifest = $this->website['manifest'];
        }
    }
}
