<?php

namespace App\Http\Livewire\Admin\Config;

use Livewire\Component;
use App\Models\Configuration;

class PrivacyPolicy extends Component
{
    public $title = 'Privacy Policy';

    public function mount() {
        $get = Configuration::first();
        $this->privacy_policy = $get->privacy_policy;
    }

    public function render() {
        return view('livewire.admin.config.privacy-policy');
    }

    public function save() {
        $exe = Configuration::first()->update([
            'privacy_policy' => $this->privacy_policy,
        ]);

        $this->emit('alert', 'Privacy Policy has been updated.');
    }
}
