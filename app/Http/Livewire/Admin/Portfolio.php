<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\Portfolio as PortfolioModel;
use App\Models\Client;
use App\Models\Service;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Helpers;

class Portfolio extends Component
{
    use WithPagination, WithFileUploads;

    public $title = 'Portfolio';
    public $searchable = [
        'services.name',
        'clients.name',
        'portfolios.name',
        'portfolios.url',
        'portfolios.description',
        'portfolios.year',
    ];
    public $search = '',
        $uid,
        $data,
        $clients,
        $paginate = 10,
        $orderBy = 'services.name',
        $order = 'asc',
        $isUpdate = false;

    protected $listeners = [
        'delete' => 'destroy',
        'changeOrder',
    ];

    protected $rules = [
        'data.service_id' => 'required',
        'data.name' => 'required',
        'data.image' => 'nullable|mimes:jpeg,bmp,png,svg,webp,jpg,ico',
        'data.url' => 'required',
        'data.description' => 'required',
        'data.year' => 'required',
    ];

    protected $messages = [
        'data.service_id.required' => 'Service is required.',
        'data.name.required' => 'Name is required.',
        'data.image.mimes' => 'Image must be a file of type: jpeg, bmp, png, svg, webp, jpg, ico.',
        'data.url.required' => 'URL is required.',
        'data.description.required' => 'Description is required.',
        'data.year.required' => 'Year is required.',
    ];

    public function mount() {
        $this->clients = Client::all();
        $this->service = Service::all();
    }

    public function render()
    {
        $sql = PortfolioModel::leftJoin('clients', 'clients.id', '=', 'portfolios.client_id')
                ->leftJoin('services', 'services.id', '=', 'portfolios.service_id')
                ->orderBy($this->orderBy, $this->order)
                ->select('portfolios.*', 'clients.name as client', 'services.name as service')
                ->latest();
        $get = $sql->paginate($this->paginate);

        // Search data
        if ($this->search != null) {
            $get = $sql;

            foreach ($this->searchable as $field) {
                $get = $get->orWhere($field, 'like', "%{$this->search}%");
            }

            $get = $get->paginate($this->paginate);

            $this->resetPage();
        }
        return view('livewire.admin.portfolio', compact('get'));
    }

    // Order by
    public function changeOrder($orderBy)
    {
        if ($this->orderBy == $orderBy) {
            $this->order = $this->order == 'desc' ? 'asc' : 'desc';
        }

        $this->orderBy = $orderBy;
    }

    // Confirm delete
    public function confirmDelete($id) {
        $this->emit('confirm', $id);
    }

    // Delete data
    public function destroy($id) {
        $exe = PortfolioModel::find($id);
        $exe->delete();

        $this->emit('alert', 'Delete data success');
    }

    // Get detail data
    public function getDetail($id) {
        // Get
        $get = PortfolioModel::find($id)->makeHidden([
            'created_at', 'updated_at', 'image'
        ])->toArray();

        // Set is update
        $this->isUpdate = true;

        // Set id
        $this->uid = $get['id'];

        unset($get['id']);
        $get['image'] = '';

        $this->data = $get;
    }

    // Save data
    public function save() {
        $this->validate();
        $message = 'data success';

        // Insert
        if (!$this->isUpdate) {
            // Upload image
            if($this->data['image']) {
                $upload = Helpers::uploadImage($this->data['image'], 'portfolio', 'portfolio');
                $this->data['image'] = $upload['filename'];
            }

            $exe = PortfolioModel::create($this->data);
            $message = 'Create ' . $message;
        }

        // Update
        else {
            $exe = PortfolioModel::find($this->uid);

            // Upload image
            if($this->data['image']) {
                $upload = Helpers::uploadImage($this->data['image'], 'portfolio', 'portfolio');
                $this->data['image'] = $upload['filename'];
            } else {
                $this->data['image'] = $exe['image'];
            }

            $exe->update($this->data);

            $message = 'Update ' . $message;
        }

        // Clear form
        $this->clearForm();

        // Emit alert
        $this->emit('alert', $message);
        $this->emit('closeModal');
    }

    // Clear form
    public function clearForm() {
        // Reset is update
        $this->isUpdate = false;
        $this->uid = '';
        $this->data = [
            'service_id' => '',
            'client_id' => '',
            'name' => '',
            'image' => '',
            'url' => '',
            'description' => '',
            'is_blank' => false,
            'year' => date('Y'),
        ];
    }
}
