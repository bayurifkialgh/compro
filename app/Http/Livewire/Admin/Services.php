<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\Service;
use Illuminate\Support\Str;
use Livewire\WithPagination;

class Services extends Component
{
    use WithPagination;

    public $title = 'Services';
    public $searchable = ['name', 'icon', 'slug', 'description'];
    public $search = '',
        $uid,
        $data,
        $paginate = 10,
        $orderBy = 'name',
        $order = 'asc',
        $isUpdate = false;

    protected $listeners = [
        'delete' => 'destroy',
        'changeOrder',
    ];

    protected $rules = [
        'data.name' => 'required',
        'data.icon' => 'required',
        'data.slug' => 'required',
        'data.description' => 'required',
    ];

    protected $messages = [
        'data.name.required' => 'Name is required.',
        'data.icon.required' => 'Icon is required.',
        'data.slug.required' => 'Slug is required.',
        'data.description.required' => 'Description is required.',
    ];

    public function render()
    {
        $sql = Service::orderBy($this->orderBy, $this->order)->latest();
        $get = $sql->paginate($this->paginate);

        // Search data
        if ($this->search != null) {
            $get = $sql;

            foreach ($this->searchable as $field) {
                $get = $get->orWhere($field, 'like', "%{$this->search}%");
            }

            $get = $get->paginate($this->paginate);

            $this->resetPage();
        }
        return view('livewire.admin.services', compact('get'));
    }

    // Order by
    public function changeOrder($orderBy)
    {
        if ($this->orderBy == $orderBy) {
            $this->order = $this->order == 'desc' ? 'asc' : 'desc';
        }

        $this->orderBy = $orderBy;
    }

    // Generate slug
    public function generateSlug() {
        $this->data['slug'] = Str::slug($this->data['name']);
    }

    // Confirm delete
    public function confirmDelete($id) {
        $this->emit('confirm', $id);
    }

    // Delete data
    public function destroy($id) {
        $exe = Service::find($id);
        $exe->delete();

        $this->emit('alert', 'Delete data success');
    }

    // Get detail data
    public function getDetail($id) {
        // Get
        $get = Service::find($id)->makeHidden([
            'created_at', 'updated_at'
        ])->toArray();

        // Set is update
        $this->isUpdate = true;

        // Set id
        $this->uid = $get['id'];

        unset($get['id']);

        $this->data = $get;
    }

    // Save data
    public function save() {
        $this->validate();
        $message = 'data success';

        // Insert
        if (!$this->isUpdate) {
            $exe = Service::create($this->data);
            $message = 'Create ' . $message;
        }

        // Update
        else {
            $exe = Service::find($this->uid);
            $exe->update($this->data);

            $message = 'Update ' . $message;
        }

        // Clear form
        $this->clearForm();

        // Emit alert
        $this->emit('alert', $message);
        $this->emit('closeModal');
    }

    // Clear form
    public function clearForm() {
        // Reset is update
        $this->isUpdate = false;
        $this->uid = '';
        $this->data = [
            'name' => '',
            'icon' => '',
            'slug' => '',
            'description' => '',
        ];
    }
}
