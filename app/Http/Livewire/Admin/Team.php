<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\Team as TeamModel;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Helpers;

class Team extends Component
{
    use WithPagination, WithFileUploads;

    public $title = 'Teams';
    public $searchable = ['name', 'description'];
    public $search = '',
        $uid,
        $data,
        $paginate = 10,
        $orderBy = 'name',
        $order = 'asc',
        $isUpdate = false;

    protected $listeners = [
        'delete' => 'destroy',
        'changeOrder',
    ];

    protected $rules = [
        'data.name' => 'required',
        'data.image' => 'nullable|mimes:jpeg,bmp,png,svg,webp,jpg,ico',
        'data.description' => 'required',
    ];

    protected $messages = [
        'data.name.required' => 'Name is required.',
        'data.image.mimes' => 'Image must be a file of type: jpeg, bmp, png, svg, webp, jpg, ico.',
        'data.description.required' => 'Description is required.',
    ];

    public function render()
    {
        $sql = TeamModel::orderBy($this->orderBy, $this->order)->latest();
        $get = $sql->paginate($this->paginate);

        // Search data
        if ($this->search != null) {
            $get = $sql;

            foreach ($this->searchable as $field) {
                $get = $get->orWhere($field, 'like', "%{$this->search}%");
            }

            $get = $get->paginate($this->paginate);

            $this->resetPage();
        }
        return view('livewire.admin.team', compact('get'));
    }

    // Order by
    public function changeOrder($orderBy)
    {
        if ($this->orderBy == $orderBy) {
            $this->order = $this->order == 'desc' ? 'asc' : 'desc';
        }

        $this->orderBy = $orderBy;
    }

    // Confirm delete
    public function confirmDelete($id) {
        $this->emit('confirm', $id);
    }

    // Delete data
    public function destroy($id) {
        $exe = TeamModel::find($id);
        $exe->delete();

        $this->emit('alert', 'Delete data success');
    }

    // Get detail data
    public function getDetail($id) {
        // Get
        $get = TeamModel::find($id)->makeHidden([
            'created_at', 'updated_at', 'image'
        ])->toArray();

        // Set is update
        $this->isUpdate = true;

        // Set id
        $this->uid = $get['id'];

        unset($get['id']);
        $get['image'] = '';
        $get['socials'] = json_decode($get['socials'], true);

        $this->data = $get;
    }

    // Save data
    public function save() {
        $this->validate();
        $message = 'data success';

        // JSON
        $this->data['socials'] = json_encode($this->data['socials']);

        // Insert
        if (!$this->isUpdate) {
            // Upload image
            if($this->data['image']) {
                $upload = Helpers::uploadImage($this->data['image'], 'team', 'team');
                $this->data['image'] = $upload['filename'];
            }

            $exe = TeamModel::create($this->data);
            $message = 'Create ' . $message;
        }

        // Update
        else {
            $exe = TeamModel::find($this->uid);

            // Upload image
            if($this->data['image']) {
                $upload = Helpers::uploadImage($this->data['image'], 'team', 'team');
                $this->data['image'] = $upload['filename'];
            } else {
                $this->data['image'] = $exe['image'];
            }

            $exe->update($this->data);

            $message = 'Update ' . $message;
        }

        // Clear form
        $this->clearForm();

        // Emit alert
        $this->emit('alert', $message);
        $this->emit('closeModal');
    }

    // Clear form
    public function clearForm() {
        // Reset is update
        $this->isUpdate = false;
        $this->uid = '';
        $this->data = [
            'name' => '',
            'image' => '',
            'socials' => '',
            'description' => '',
        ];
    }
}
