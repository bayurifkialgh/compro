<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Service as ServiceModel;
use App\Models\TechExpertise;

class Service extends Component
{
    public $title = 'Service';

    public $service;
    public $tech_expertises = [];

    public function mount($slug) {
        $this->service = ServiceModel::where('slug', $slug)->first();

        // If the service is found, get the tech expertises
        if($this->service) {
            $this->tech_expertises = TechExpertise::where('service_id', $this->service->id)->get();
        }
    }

    public function render()
    {
        return view('livewire.service')->layout(\App\View\Components\AppLayout::class);
    }
}
