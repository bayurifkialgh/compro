<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\AboutUs;
use App\Models\Service;
use App\Models\Client;
use App\Models\TechExpertise;

class Home extends Component
{
    public $title = 'Home';

    public $aboutUs;
    public $services;
    public $clients;
    public $techExpertises;

    public function mount() {
        $this->aboutUs = AboutUs::first();
        $this->services = Service::all();
        $this->clients = Client::all();
        $this->techExpertises = TechExpertise::all();
    }

    public function render()
    {
        return view('livewire.home')->layout(\App\View\Components\AppLayout::class);
    }
}
