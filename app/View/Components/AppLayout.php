<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Models\Configuration;
use App\Models\Service;
use App\Models\AboutUs;

class AppLayout extends Component
{
    public $metas;
    public $websites;
    public $socials;
    public $services;
    public $aboutUs;
    public $routeName;

    public function render()
    {
        $configurations = Configuration::first();

        $this->metas = json_decode($configurations->metas, true);
        $this->websites = json_decode($configurations->website, true);
        $this->socials = json_decode($configurations->socials, true);
        $this->services = Service::all();
        $this->aboutUs = AboutUs::first();
        $this->routeName = request()->route()->getName();

        return view('layouts.app-main');
    }
}
