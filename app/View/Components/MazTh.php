<?php

namespace App\View\Components;

use Illuminate\View\Component;

class MazTh extends Component
{
    public $orderby;
    public $order;
    public $field;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($orderby, $order, $field)
    {
        $this->orderby = $orderby;
        $this->order = $order;
        $this->field = $field;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.maz-th');
    }
}
