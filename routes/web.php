<?php

use Illuminate\Support\Facades\Route;

Route::get('/', App\Http\Livewire\Home::class)->name('home');
Route::get('/service/{slug}', App\Http\Livewire\Service::class)->name('service');

// Profile
Route::get('/about-us', App\Http\Livewire\AboutUs::class)->name('about-us');
Route::get('/team', App\Http\Livewire\Team::class)->name('team');
Route::get('/portfolio', App\Http\Livewire\Portfolio::class)->name('portfolio');

Route::get('/contact-us', App\Http\Livewire\ContactUs::class)->name('contact-us');
Route::get('/term-condition', App\Http\Livewire\TermCond::class)->name('term-condition');
Route::get('/privacy-policy', App\Http\Livewire\PrivacyPolicy::class)->name('privacy-policy');

Route::group([
    'middleware' => ['auth:sanctum', 'verified'],
    'prefix' => 'admin',
    'as' => 'admin.'
], function() {

    Route::get('/dashboard', App\Http\Livewire\Admin\Dashboard::class)->name('dashboard');
    Route::get('/about-us', App\Http\Livewire\Admin\AboutUs::class)->name('about-us');
    Route::get('/tech-expertises', App\Http\Livewire\Admin\TechExpertise::class)->name('tech-expertises');
    Route::get('/services', App\Http\Livewire\Admin\Services::class)->name('services');
    Route::get('/clients', App\Http\Livewire\Admin\Client::class)->name('clients');
    Route::get('/portfolio', App\Http\Livewire\Admin\Portfolio::class)->name('portfolios');
    Route::get('/teams', App\Http\Livewire\Admin\Team::class)->name('teams');
    Route::get('/contact-us', App\Http\Livewire\Admin\ContactUs::class)->name('contact');

    // Configurations
    Route::group([
        'prefix' => 'configuration',
        'as' => 'configuration.'
    ], function() {
        Route::get('/website', App\Http\Livewire\Admin\Config\Website::class)->name('website');
        Route::get('/meta', App\Http\Livewire\Admin\Config\Meta::class)->name('meta');
        Route::get('/social', App\Http\Livewire\Admin\Config\Social::class)->name('social');
        Route::get('/privacy-policy', App\Http\Livewire\Admin\Config\PrivacyPolicy::class)->name('privacy-policy');
        Route::get('/term-cond', App\Http\Livewire\Admin\Config\TermCond::class)->name('term-cond');
    });

});
