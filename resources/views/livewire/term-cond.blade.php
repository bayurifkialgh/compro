<div>
    <x-slot name="title">
        {{ $title }}
    </x-slot>
    <div class="section py-0">
        <div class="container mt-n10 mt-lg-n12 z-2 mb-5">
            <div class="row justify-content-center">
                <div class="col-12 col-md-10 mt-5">
                    <div class="card shadow mb-6">
                        <div class="card-body px-5 py-5 text-center text-md-left">
                            <div class="row align-items-center">
                                <div class="col-md-12">
                                    <x-markdown>
                                        {{ $term_condition }}
                                    </x-markdown>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
