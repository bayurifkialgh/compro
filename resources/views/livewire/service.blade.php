<div>
    <x-slot name="title">
        @if($this->service)
            {{ $this->service->name }}
        @else
            {{ $title }}
        @endif
    </x-slot>
    <div class="section py-0">
        <div class="container mt-n10 mt-lg-n12 z-2 mb-5">
            <div class="row justify-content-center">
                <div class="col-12 col-md-12 mt-5">
                    <div class="card shadow mb-6">
                        <div class="card-body px-5 py-5 text-center text-md-left">
                            <div class="row align-items-center">
                                <div class="col-md-12">
                                    @if($this->service)
                                        <h2 class="mb-3">
                                            <span class="{{ $this->service->icon }}"></span>
                                            {{ $this->service->name }}
                                        </h2>
                                        <div class="row align-items-center mb-5 mb-lg-7">
                                            <div class="col-12 col-lg-12 mb-5">
                                                <p class="mb-4 lead">
                                                    {{ $this->service->description }}
                                                </p>
                                            </div>
                                            @foreach($tech_expertises as $te)
                                                <div class="col-6 col-md-3 text-center mb-4">
                                                    <div class="icon icon-shape icon-lg bg-white shadow-lg border-light rounded-circle mb-4">
                                                        <span class="{{ $te->icon }} text-tertiary"></span>
                                                    </div>
                                                    <h5 class="fw-bolder">{{ $te->name }}</h5>
                                                    <p class="text-gray"></p>
                                                </div>
                                            @endforeach
                                        </div>
                                    @else
                                        <h2 class="mb-5">Page not found</h2>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

