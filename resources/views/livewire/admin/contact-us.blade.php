<div>
    <x-slot name="title">
        {{ $title }}
    </x-slot>
    <x-slot name="header">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>{{ $title }}</h3>
            </div>
            <div class="col-12 col-md-6 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Cients</li>
                    </ol>
                </nav>
            </div>
        </div>
    </x-slot>

    <section class="section">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                        <select wire:model.lazy="paginate" class="form-control sm">
                            <option value="5">5 entries per page</option>
                            <option value="10">10 entries per page</option>
                            <option value="50">50 entries per page</option>
                            <option value="100">100 entries per page</option>
                        </select>
                    </div>
                    <div class="col-lg-4 col-md-4"></div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="ti ti-search"></i>
                            </span>
                            <input wire:model="search" type="text" class="form-control sm"
                                placeholder="Search......" autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-lg table-striped mb-0">
                        <thead>
                            <tr>
                                <x-maz-th :orderby="$orderBy" :order="$order" field="services.name">
                                    Service
                                </x-maz-th>
                                <x-maz-th :orderby="$orderBy" :order="$order" field="contact_us.name">
                                    Name
                                </x-maz-th>
                                <x-maz-th :orderby="$orderBy" :order="$order" field="contact_us.company">
                                    Company
                                </x-maz-th>
                                <x-maz-th :orderby="$orderBy" :order="$order" field="contact_us.email">
                                    Email
                                </x-maz-th>
                                <x-maz-th :orderby="$orderBy" :order="$order" field="contact_us.contact">
                                    Contact
                                </x-maz-th>
                                <x-maz-th :orderby="$orderBy" :order="$order" field="contact_us.project">
                                    Project
                                </x-maz-th>
                                <x-maz-th :orderby="$orderBy" :order="$order" field="contact_us.message">
                                    Message
                                </x-maz-th>
                                <x-maz-th :orderby="$orderBy" :order="$order" field="contact_us.created_at">
                                    Created at
                                </x-maz-th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($get as $d)
                                <tr>
                                    <td>{{ $d->service }}</td>
                                    <td>{{ $d->name }}</td>
                                    <td>{{ $d->company }}</td>
                                    <td>{{ $d->email }}</td>
                                    <td>{{ $d->contact }}</td>
                                    <td>{{ $d->project }}</td>
                                    <td>{{ $d->message }}</td>
                                    <td>{{ $d->created_at->format('d F Y H:i:s') }}</td>
                                </tr>
                            @empty
                                <tr class="text-center">
                                    <td colspan="10">Data empty</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                    {{ $get->links('components.pagination') }}
                </div>
            </div>
        </div>
    </section>
</div>
