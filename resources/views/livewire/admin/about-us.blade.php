<div>
    <x-slot name="title">
        {{ $title ?? '' }}
    </x-slot>
    <x-slot name="header">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>About us</h3>
            </div>
            <div class="col-12 col-md-6 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">About us</li>
                    </ol>
                </nav>
            </div>
        </div>
    </x-slot>

    <section class="section">
        <div class="card">
            <div class="card-body">
                <form wire:submit.prevent="save">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="app-name">Company Name</label>
                                <input type="text" id="app-name" class="form-control" placeholder="Company Name" wire:model.lazy="data.name">
                                <x-maz-input-error for="data.name" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="app-slogan">Slogan</label>
                                <input type="text" id="app-slogan" class="form-control" placeholder="Slogan" wire:model.lazy="data.slogan">
                                <x-maz-input-error for="data.slogan" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="app-phone">Phone Number</label>
                                <input type="number" id="app-phone" class="form-control" placeholder="Phone" wire:model.lazy="data.phone">
                                <x-maz-input-error for="data.phone" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="app-logo">Logo</label>
                                <div class="preview mb-2 mt-1">
                                    <img src="{{ url('storage/about-us/' . $logo) }}" width="100px" />
                                </div>
                                <input type="file" id="app-logo" class="form-control" placeholder="Logo" wire:model.lazy="data.logo">
                                <x-maz-input-error for="data.logo" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="app-description">About us</label>
                                <textarea id="app-description" class="form-control" rows="5" placeholder="Description" wire:model.lazy="data.description"></textarea>
                                <x-maz-input-error for="data.description" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="app-address">Address</label>
                                <textarea id="app-address" class="form-control" rows="5" placeholder="Address" wire:model.lazy="data.address"></textarea>
                                <x-maz-input-error for="data.address" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="app-vision">Vision (Markdown)</label>
                                <textarea id="app-vision" class="form-control" placeholder="Vision" rows="10" wire:model="data.vision"></textarea>
                                <x-maz-input-error for="data.vision" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="app-vision">Vision Preview</label>
                                <x-markdown>
                                    {{ $data['vision'] }}
                                </x-markdown>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="app-mision">Mission (Markdown)</label>
                                <textarea id="app-mision" class="form-control" placeholder="Mission" rows="10" wire:model="data.mision"></textarea>
                                <x-maz-input-error for="data.mision" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="app-vision">Mission Preview</label>
                                <x-markdown>
                                    {{ $data['mision'] }}
                                </x-markdown>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="float-end">
                                <button class="btn btn-primary">
                                    <i class="bi bi-send"></i>
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
