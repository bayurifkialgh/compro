<div>
    <x-slot name="title">
        {{ $title }}
    </x-slot>
    <x-slot name="header">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>{{ $title }}</h3>
            </div>
            <div class="col-12 col-md-6 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Cients</li>
                    </ol>
                </nav>
            </div>
        </div>
    </x-slot>

    <section class="section">
        <div class="card">
            <x-crud-head />
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-lg table-striped mb-0">
                        <thead>
                            <tr>
                                <x-maz-th :orderby="$orderBy" :order="$order" field="services.name">
                                    Service
                                </x-maz-th>
                                <x-maz-th :orderby="$orderBy" :order="$order" field="portfolios.name">
                                    Name
                                </x-maz-th>
                                <x-maz-th :orderby="$orderBy" :order="$order" field="clients.name">
                                    Client
                                </x-maz-th>
                                <x-maz-th :orderby="$orderBy" :order="$order" field="portfolios.image">
                                    Image
                                </x-maz-th>
                                <x-maz-th :orderby="$orderBy" :order="$order" field="portfolios.url">
                                    Url
                                </x-maz-th>
                                <x-maz-th :orderby="$orderBy" :order="$order" field="portfolios.is_blank">
                                    Is Blank
                                </x-maz-th>
                                <x-maz-th :orderby="$orderBy" :order="$order" field="portfolios.description">
                                    Description
                                </x-maz-th>
                                <x-maz-th :orderby="$orderBy" :order="$order" field="portfolios.year">
                                    Year
                                </x-maz-th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($get as $d)
                                <tr>
                                    <td>{{ $d->service }}</td>
                                    <td>{{ $d->name }}</td>
                                    <td>{{ $d->client }}</td>
                                    <td>{{ $d->image }}</td>
                                    <td>{{ $d->url }}</td>
                                    <td>{{ $d->is_blank }}</td>
                                    <td>{{ $d->description }}</td>
                                    <td>{{ $d->year }}</td>
                                    <td class="text-center">
                                        <a class="btn btn-primary btn-sm"
                                            wire:click="getDetail('{{ $d->id }}')" data-bs-toggle="modal"
                                            data-bs-target="#modal-crud">
                                            <i class="bi bi-pencil"></i> Update
                                        </a>
                                        <button class="btn btn-danger btn-sm"
                                            wire:click="confirmDelete('{{ $d->id }}')">
                                            <i class="bi bi-trash"></i> Delete
                                        </button>
                                    </td>
                                </tr>
                            @empty
                                <tr class="text-center">
                                    <td colspan="10">Data empty</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                    {{ $get->links('components.pagination') }}
                </div>
            </div>
        </div>
    </section>

    <div wire:ignore.self class="modal fade" id="modal-crud" tabindex="-1" aria-labelledby="modal-sub-menu-title"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <form wire:submit.prevent="save">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal-sub-menu-title">
                            {{ $isUpdate ? 'Update' : 'Create' }}
                            {{ $title }}
                        </h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" id="name" class="form-control" placeholder="Name" wire:model.lazy="data.name">
                                    <x-maz-input-error for="data.name" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="client_id">Client</label>
                                    <select id="client_id" class="form-control" wire:model.lazy="data.client_id">
                                        <option value="">Select Client</option>
                                        @foreach ($clients as $c)
                                            <option value="{{ $c->id }}">{{ $c->name }}</option>
                                        @endforeach
                                    </select>
                                    <x-maz-input-error for="data.client_id" />
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="service_id">Service</label>
                                    <select id="service_id" class="form-control" wire:model.lazy="data.service_id">
                                        <option value="">Select Service</option>
                                        @foreach ($service as $srv)
                                            <option value="{{ $srv->id }}">{{ $srv->name }}</option>
                                        @endforeach
                                    </select>
                                    <x-maz-input-error for="data.service_id" />
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="image">Image</label>
                                    <input type="file" id="image" class="form-control" placeholder="Image" wire:model.lazy="data.image">
                                    <x-maz-input-error for="data.image" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="url">URL</label>
                                    <input type="text" id="url" class="form-control" placeholder="URL" wire:model.lazy="data.url">
                                    <x-maz-input-error for="data.url" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="is_blank">Is Blank</label>
                                    <select id="is_blank" class="form-control" wire:model.lazy="data.is_blank">
                                        <option value="0">False</option>
                                        <option value="1">True</option>
                                    </select>
                                    <x-maz-input-error for="data.is_blank" />
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea id="description" class="form-control" placeholder="Description" wire:model.lazy="data.description"></textarea>
                                    <x-maz-input-error for="data.description" />
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="year">Description</label>
                                    <input type="number" min="1900" max="2099" step="1" id="year" class="form-control" placeholder="Year" wire:model.lazy="data.year">
                                    <x-maz-input-error for="data.year" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
