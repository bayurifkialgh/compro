<div>
    <x-slot name="title">
        {{ $title ?? '' }}
    </x-slot>
    <x-slot name="header">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>Website</h3>
            </div>
            <div class="col-12 col-md-6 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item">Configuration</li>
                        <li class="breadcrumb-item active" aria-current="page">Website</li>
                    </ol>
                </nav>
            </div>
        </div>
    </x-slot>

    <section class="section">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Configuration</h4>
            </div>
            <div class="card-body">
                <form wire:submit.prevent="save">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="app-name">App Name</label>
                                <input type="text" id="app-name" class="form-control" placeholder="App name" wire:model.lazy="app_name">
                                <x-maz-input-error for="app_name" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="app-icon">Default Icon</label>
                                <div class="preview mb-2 mt-1">
                                    <img src="{{ url('storage/websites') . '/' . $website['app_icon'] }}" width="100px" />
                                </div>
                                <input type="file" id="app-icon" class="form-control" wire:model.lazy="app_icon">
                                <x-maz-input-error for="app_icon" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="app-icon_16x16">Icon 16x16</label>
                                <div class="preview mb-2 mt-1">
                                    <img src="{{ url('storage/websites') . '/' . $website['icon_16x16'] }}" width="100px" />
                                </div>
                                <input type="file" id="app-icon_16x16" class="form-control" wire:model.lazy="icon_16x16">
                                <x-maz-input-error for="icon_16x16" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="app-icon_32x32">Icon 32x32</label>
                                <div class="preview mb-2 mt-1">
                                    <img src="{{ url('storage/websites') . '/' . $website['icon_32x32'] }}" width="100px" />
                                </div>
                                <input type="file" id="app-icon_32x32" class="form-control" wire:model.lazy="icon_32x32">
                                <x-maz-input-error for="icon_32x32" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="app-app_apple_icon">Apple Icon</label>
                                <div class="preview mb-2 mt-1">
                                    <img src="{{ url('storage/websites') . '/' . $website['app_apple_icon'] }}" width="100px" />
                                </div>
                                <input type="file" id="app-app_apple_icon" class="form-control" wire:model.lazy="app_apple_icon">
                                <x-maz-input-error for="app_apple_icon" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="app-manifest">Web Manifest</label>
                                <div class="preview">
                                    <a href="{{ url('storage/websites') . '/' . $website['manifest'] }}" target="_blank">
                                        {{ url('storage/websites') . '/' . $website['manifest'] }}
                                    </a>
                                </div>
                                <input type="file" id="app-manifest" class="form-control" wire:model.lazy="manifest">
                                <x-maz-input-error for="manifest" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="float-end">
                                <button class="btn btn-primary">
                                    <i class="bi bi-send"></i>
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
