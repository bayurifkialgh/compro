<div>
    <x-slot name="title">
        {{ $title ?? '' }}
    </x-slot>
    <x-slot name="header">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>Privacy Policy</h3>
            </div>
            <div class="col-12 col-md-6 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item">Configuration</li>
                        <li class="breadcrumb-item active" aria-current="page">Privacy Policy</li>
                    </ol>
                </nav>
            </div>
        </div>
    </x-slot>

    <section class="section">
        <div class="card">
            <div class="card-body">
                <form wire:submit.prevent="save">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="app-privacy_policy">Privacy Policy (Markdown)</label>
                                <textarea id="app-privacy_policy" class="form-control" placeholder="Term & Condition" rows="15" wire:model="privacy_policy"></textarea>
                                <x-maz-input-error for="privacy_policy" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="app-phone">Preview</label>
                                <x-markdown>
                                    {{ $privacy_policy }}
                                </x-markdown>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="float-end">
                                <button class="btn btn-primary">
                                    <i class="bi bi-send"></i>
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
