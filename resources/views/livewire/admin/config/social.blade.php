<div>
    <x-slot name="title">
        {{ $title ?? '' }}
    </x-slot>
    <x-slot name="header">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>Meta data</h3>
            </div>
            <div class="col-12 col-md-6 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item">Configuration</li>
                        <li class="breadcrumb-item active" aria-current="page">Social</li>
                    </ol>
                </nav>
            </div>
        </div>
    </x-slot>

    <section class="section">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Configuration</h4>
            </div>
            <div class="card-body">
                <form wire:submit.prevent="save">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="app-whatsapp">Whatsapp</label>
                                <input type="text" id="app-whatsapp" class="form-control" placeholder="Whatsapp" wire:model.lazy="data.whatsapp">
                                <x-maz-input-error for="data.whatsapp" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="app-phone">Phone</label>
                                <input type="text" id="app-phone" class="form-control" placeholder="Phone" wire:model.lazy="data.phone">
                                <x-maz-input-error for="data.phone" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="app-github">Github</label>
                                <input type="text" id="app-github" class="form-control" placeholder="Github" wire:model.lazy="data.github">
                                <x-maz-input-error for="data.github" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="app-twitter">Twitter</label>
                                <input type="text" id="app-twitter" class="form-control" placeholder="Twitter" wire:model.lazy="data.twitter">
                                <x-maz-input-error for="data.twitter" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="app-facebook">Facebook</label>
                                <input type="text" id="app-facebook" class="form-control" placeholder="Facebook" wire:model.lazy="data.facebook">
                                <x-maz-input-error for="data.facebook" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="app-linkedin">Linkedin</label>
                                <input type="text" id="app-linkedin" class="form-control" placeholder="Linkedin" wire:model.lazy="data.linkedin">
                                <x-maz-input-error for="data.linkedin" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="app-youtube">Youtube</label>
                                <input type="text" id="app-youtube" class="form-control" placeholder="Youtube" wire:model.lazy="data.youtube">
                                <x-maz-input-error for="data.youtube" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="app-instagram">Instagram</label>
                                <input type="text" id="app-instagram" class="form-control" placeholder="Instagram" wire:model.lazy="data.instagram">
                                <x-maz-input-error for="data.instagram" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="float-end">
                                <button class="btn btn-primary">
                                    <i class="bi bi-send"></i>
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
