<div>
    <x-slot name="title">
        {{ $title ?? '' }}
    </x-slot>
    <x-slot name="header">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>Meta data</h3>
            </div>
            <div class="col-12 col-md-6 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item">Configuration</li>
                        <li class="breadcrumb-item active" aria-current="page">Meta data</li>
                    </ol>
                </nav>
            </div>
        </div>
    </x-slot>

    <section class="section">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Configuration</h4>
            </div>
            <div class="card-body">
                <form wire:submit.prevent="save">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="app-title">Title</label>
                                <input type="text" id="app-title" class="form-control" placeholder="Meta title" wire:model.lazy="data.title">
                                <x-maz-input-error for="data.title" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="app-author">Author</label>
                                <input type="text" id="app-author" class="form-control" placeholder="Author" wire:model.lazy="data.author">
                                <x-maz-input-error for="data.author" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="app-description">Description</label>
                                <textarea type="text" id="app-description" class="form-control" placeholder="Description" wire:model.lazy="data.description"></textarea>
                                <x-maz-input-error for="data.description" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="app-keywords">Keywords</label>
                                <textarea type="text" id="app-keywords" class="form-control" placeholder="Keywords" wire:model.lazy="data.keywords"></textarea>
                                <x-maz-input-error for="data.keywords" />
                            </div>
                        </div>


                        <div class="col-md-12">
                            <h4>Facebook Open Graph</h4>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="app-og:title">Open Graph Title</label>
                                <input type="text" id="app-og:title" class="form-control" placeholder="Open graph title" wire:model.lazy="data.og:title">
                                <x-maz-input-error for="data.og:title" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="app-og:type">Open Graph Type</label>
                                <input type="text" id="app-og:type" class="form-control" placeholder="Open graph type" wire:model.lazy="data.og:type">
                                <x-maz-input-error for="data.og:type" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="app-og:url">Open Graph URL</label>
                                <input type="text" id="app-og:url" class="form-control" placeholder="Open graph url" wire:model.lazy="data.og:url">
                                <x-maz-input-error for="data.og:url" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="app-og:image">Open Graph Image URL</label>
                                <input type="text" id="app-og:image" class="form-control" placeholder="Open graph image" wire:model.lazy="data.og:image">
                                <x-maz-input-error for="data.og:image" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="app-og:description">Open Graph Description</label>
                                <textarea type="text" id="app-og:description" class="form-control" placeholder="Open graph description" wire:model.lazy="data.og:description"></textarea>
                                <x-maz-input-error for="data.og:description" />
                            </div>
                        </div>


                        <div class="col-md-12">
                            <h4>Twitter</h4>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="app-twitter:card">Twitter Card</label>
                                <input type="text" id="app-twitter:card" class="form-control" placeholder="Twitter card" wire:model.lazy="data.twitter:card">
                                <x-maz-input-error for="data.twitter:card" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="app-twitter:url">Twitter URL</label>
                                <input type="text" id="app-twitter:url" class="form-control" placeholder="Twitter url" wire:model.lazy="data.twitter:url">
                                <x-maz-input-error for="data.twitter:url" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="app-twitter:image">Twitter Image URL</label>
                                <input type="text" id="app-twitter:image" class="form-control" placeholder="Twitter image" wire:model.lazy="data.twitter:image">
                                <x-maz-input-error for="data.twitter:image" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="app-twitter:description">Twitter Description</label>
                                <textarea type="text" id="app-twitter:description" class="form-control" placeholder="Twitter description" wire:model.lazy="data.twitter:description"></textarea>
                                <x-maz-input-error for="data.twitter:description" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="float-end">
                                <button class="btn btn-primary">
                                    <i class="bi bi-send"></i>
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
