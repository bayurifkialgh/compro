<div>
    <x-slot name="title">
        {{ $title }}
    </x-slot>
    <x-slot name="header">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>{{ $title }}</h3>
            </div>
            <div class="col-12 col-md-6 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Tech Expertises</li>
                    </ol>
                </nav>
            </div>
        </div>
    </x-slot>

    <section class="section">
        <div class="card">
            <x-crud-head />
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-lg table-striped mb-0">
                        <thead>
                            <tr>
                                <x-maz-th :orderby="$orderBy" :order="$order" field="services.name">
                                    Service
                                </x-maz-th>
                                <x-maz-th :orderby="$orderBy" :order="$order" field="tech_expertises.name">
                                    Name
                                </x-maz-th>
                                <x-maz-th :orderby="$orderBy" :order="$order" field="tech_expertises.icon">
                                    Icon
                                </x-maz-th>
                                <x-maz-th :orderby="$orderBy" :order="$order" field="tech_expertises.slug">
                                    Slug
                                </x-maz-th>
                                <x-maz-th :orderby="$orderBy" :order="$order" field="tech_expertises.description">
                                    Description
                                </x-maz-th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($get as $d)
                                <tr>
                                    <td>{{ $d->service }}</td>
                                    <td>{{ $d->name }}</td>
                                    <td>{{ $d->icon }}</td>
                                    <td>{{ $d->slug }}</td>
                                    <td>{{ $d->description }}</td>
                                    <td class="text-center">
                                        <a class="btn btn-primary btn-sm"
                                            wire:click="getDetail('{{ $d->id }}')" data-bs-toggle="modal"
                                            data-bs-target="#modal-crud">
                                            <i class="bi bi-pencil"></i> Update
                                        </a>
                                        <button class="btn btn-danger btn-sm"
                                            wire:click="confirmDelete('{{ $d->id }}')">
                                            <i class="bi bi-trash"></i> Delete
                                        </button>
                                    </td>
                                </tr>
                            @empty
                                <tr class="text-center">
                                    <td colspan="10">Data empty</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                    {{ $get->links('components.pagination') }}
                </div>
            </div>
        </div>
    </section>

    <div wire:ignore.self class="modal fade" id="modal-crud" tabindex="-1" aria-labelledby="modal-sub-menu-title"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <form wire:submit.prevent="save">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal-sub-menu-title">
                            {{ $isUpdate ? 'Update' : 'Create' }}
                            {{ $title }}
                        </h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="service_id">Service</label>
                                    <select id="service_id" class="form-control" wire:model.lazy="data.service_id">
                                        <option value="">Select Service</option>
                                        @foreach($service as $srv)
                                            <option value="{{ $srv->id }}">{{ $srv->name }}</option>
                                        @endforeach
                                    </select>
                                    <x-maz-input-error for="data.service_id" />
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" id="name" class="form-control" placeholder="Name" wire:model="data.name" wire:keyup="generateSlug">
                                    <x-maz-input-error for="data.name" />
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="slug">Slug</label>
                                    <input type="text" id="slug" class="form-control" placeholder="slug" wire:model.lazy="data.slug">
                                    <x-maz-input-error for="data.slug" />
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="icon">Icon</label>
                                    <input type="text" id="icon" class="form-control" placeholder="Icon" wire:model.lazy="data.icon">
                                    <x-maz-input-error for="data.icon" />
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea id="description" class="form-control" placeholder="Description" wire:model.lazy="data.description"></textarea>
                                    <x-maz-input-error for="data.description" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
