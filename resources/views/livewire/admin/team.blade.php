<div>
    <x-slot name="title">
        {{ $title }}
    </x-slot>
    <x-slot name="header">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>{{ $title }}</h3>
            </div>
            <div class="col-12 col-md-6 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Teams</li>
                    </ol>
                </nav>
            </div>
        </div>
    </x-slot>

    <section class="section">
        <div class="card">
            <x-crud-head />
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-lg table-striped mb-0">
                        <thead>
                            <tr>
                                <x-maz-th :orderby="$orderBy" :order="$order" field="name">
                                    Name
                                </x-maz-th>
                                <x-maz-th :orderby="$orderBy" :order="$order" field="image">
                                    Image
                                </x-maz-th>
                                <th>
                                    Socials
                                </th>
                                <x-maz-th :orderby="$orderBy" :order="$order" field="description">
                                    Description
                                </x-maz-th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($get as $d)
                                <tr>
                                    <td>{{ $d->name }}</td>
                                    <td>{{ $d->image }}</td>
                                    <td>
                                        @if(json_decode($d->socials, true))
                                            @foreach(json_decode($d->socials, true) as $k => $v)
                                                {{ ucfirst($k) }} : {{ '@'.$v}} <br>
                                            @endforeach
                                        @endif
                                    </td>
                                    <td>{{ $d->description }}</td>
                                    <td class="text-center">
                                        <a class="btn btn-primary btn-sm"
                                            wire:click="getDetail('{{ $d->id }}')" data-bs-toggle="modal"
                                            data-bs-target="#modal-crud">
                                            <i class="bi bi-pencil"></i> Update
                                        </a>
                                        <button class="btn btn-danger btn-sm"
                                            wire:click="confirmDelete('{{ $d->id }}')">
                                            <i class="bi bi-trash"></i> Delete
                                        </button>
                                    </td>
                                </tr>
                            @empty
                                <tr class="text-center">
                                    <td colspan="10">Data empty</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                    {{ $get->links('components.pagination') }}
                </div>
            </div>
        </div>
    </section>

    <div wire:ignore.self class="modal fade" id="modal-crud" tabindex="-1" aria-labelledby="modal-sub-menu-title"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <form wire:submit.prevent="save">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal-sub-menu-title">
                            {{ $isUpdate ? 'Update' : 'Create' }}
                            {{ $title }}
                        </h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" id="name" class="form-control" placeholder="Name" wire:model.lazy="data.name">
                                    <x-maz-input-error for="data.name" />
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="image">Image</label>
                                    <input type="file" id="image" class="form-control" placeholder="Image" wire:model.lazy="data.image">
                                    <x-maz-input-error for="data.image" />
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea id="description" class="form-control" rows="3" placeholder="Description" wire:model.lazy="data.description"></textarea>
                                    <x-maz-input-error for="data.description" />
                                </div>
                            </div>
                            <div class="col-md-12">
                                <h4>Social Media</h4>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="facebook">Facebook</label>
                                    <input type="text" id="facebook" class="form-control" placeholder="Facebook" wire:model.lazy="data.socials.facebook">
                                    <x-maz-input-error for="data.socials.facebook" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="twitter">Twitter</label>
                                    <input type="text" id="twitter" class="form-control" placeholder="Twitter" wire:model.lazy="data.socials.twitter">
                                    <x-maz-input-error for="data.socials.twitter" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="discord">Discord</label>
                                    <input type="text" id="discord" class="form-control" placeholder="Discord" wire:model.lazy="data.socials.discord">
                                    <x-maz-input-error for="data.socials.discord" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="instagram">Instagram</label>
                                    <input type="text" id="instagram" class="form-control" placeholder="Instagram" wire:model.lazy="data.socials.instagram">
                                    <x-maz-input-error for="data.socials.instagram" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="linkedin">Linkedin</label>
                                    <input type="text" id="linkedin" class="form-control" placeholder="Linkedin" wire:model.lazy="data.socials.linkedin">
                                    <x-maz-input-error for="data.socials.linkedin" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="github">GitHub</label>
                                    <input type="text" id="github" class="form-control" placeholder="GitHub" wire:model.lazy="data.socials.github">
                                    <x-maz-input-error for="data.socials.github" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
