<div>
    <x-slot name="title">
        {{ $title }}
    </x-slot>
    <div class="section py-0">
        <div class="container mt-n10 mt-lg-n12 z-2 mb-5">
            <div class="row justify-content-center">
                <div class="col-12 col-md-12 mt-5">
                    <div class="card shadow mb-6">
                        <div class="card-body px-5 py-5 text-center text-md-left">
                            <div class="row align-items-center">
                                <div class="col-md-12">
                                    <h2 class="mb-3">About Us</h2>
                                    <p class="mb-0">
                                        {{ $aboutUs['description'] }}
                                    </p>
                                </div>
                            </div>
                            <div class="row align-items-center mt-5">
                                <div class="col-md-12">
                                    <h2 class="mb-3">Vision</h2>
                                    <p class="mb-0">
                                        <x-markdown>
                                            {{ $aboutUs['vision'] }}
                                        </x-markdown>
                                    </p>
                                </div>
                                <div class="col-md-12">
                                    <h2 class="mb-3">Mission</h2>
                                    <p class="mb-0">
                                        <x-markdown>
                                            {{ $aboutUs['mision'] }}
                                        </x-markdown>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

