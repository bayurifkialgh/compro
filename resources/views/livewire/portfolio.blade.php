<div>
    <x-slot name="title">
        {{ $title }}
    </x-slot>
    <div class="section py-0">
        <div class="container mt-n10 mt-lg-n12 z-2 mb-5">
            <div class="row justify-content-center">
                <div class="col-12 col-md-12 mt-5">
                    <div class="card shadow mb-6">
                        <div class="card-body px-5 py-5 text-center text-md-left">
                            <div class="row align-items-center">
                                <div class="col-md-12">
                                    <h2 class="mb-3">Portfolio</h2>
                                    <div class="row">
                                        @foreach ($portfolios as $pt)
                                            <div class="col-6 col-md-4">
                                                <a href="{{ $pt->url }}" target="_blank" class="page-preview scale-up-hover-2">
                                                    <img class="shadow-lg rounded scale"
                                                        loading="lazy"
                                                        src="{{ url('storage/portfolio/' . $pt->image) }}"
                                                        alt="{{ $pt->name }} preview">
                                                    <div class="text-center show-on-hover">
                                                        <h3 class="h6 m-0 text-center text-white">{{ $pt->name }}
                                                            <span class="fas fa-external-link-alt ms-2"></span>
                                                        </h3>
                                                    </div>
                                                </a>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

