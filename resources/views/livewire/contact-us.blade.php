<div>
    <x-slot name="title">
        {{ $title }}
    </x-slot>
    <div class="section py-0">
        <div class="container mt-n10 mt-lg-n12 z-2 mb-5">
            <div class="row justify-content-center">
                <div class="col-12 col-md-10 mt-5">
                    <div class="card shadow mb-6">
                        <div class="card-body px-5 py-5 text-center text-md-left">
                            <div class="row align-items-center">
                                <div class="col-md-12">
                                    <h2 class="h1 fw-light mb-3">Have a project in mind?</h2>
                                    <p class="lead">
                                        Let’s start a discussion!
                                    </p>
                                    <div class="row justify-content-center mb-6">
                                        <div class="col-md-10 col-xl-9">
                                            <div class="position-relative">
                                                <form class="rounded p-4 mb-2" wire:submit.prevent="save">
                                                    <div class="row mb-4">
                                                        <div class="col-6 col-md-6">
                                                            <label class="fw-normal">Name</label>
                                                            <div class="text-muted">
                                                                <input type="text" class="form-control" wire:model.lazy="data.name">
                                                            </div>
                                                            <x-maz-input-error for="data.name" />
                                                        </div>
                                                        <div class="col-6 col-md-6">
                                                            <label class="fw-normal">Company</label>
                                                            <div class="text-muted">
                                                                <input type="text" class="form-control" wire:model.lazy="data.company">
                                                            </div>
                                                            <x-maz-input-error for="data.company" />
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4">
                                                        <div class="col-6 col-md-6">
                                                            <label class="fw-normal">Email</label>
                                                            <div class="text-muted">
                                                                <input type="email" class="form-control" wire:model.lazy="data.email">
                                                            </div>
                                                            <x-maz-input-error for="data.email" />
                                                        </div>
                                                        <div class="col-6 col-md-6">
                                                            <label class="fw-normal">Contact</label>
                                                            <div class="text-muted">
                                                                <input type="number" class="form-control" wire:model.lazy="data.contact">
                                                            </div>
                                                            <x-maz-input-error for="data.contact" />
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4">
                                                        <div class="col-12 col-md-12">
                                                            <label class="fw-normal">Service</label>
                                                            <div class="text-muted">
                                                                <select class="form-control" wire:model.lazy="data.service_id">
                                                                    <option value="">--Select Service--</option>
                                                                    @foreach($services as $srv)
                                                                        <option value="{{ $srv->id }}">{{ $srv->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <x-maz-input-error for="data.service_id" />
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4">
                                                        <div class="col-12 col-md-12">
                                                            <label class="fw-normal">Project</label>
                                                            <div class="text-muted">
                                                                <input type="text" class="form-control" wire:model.lazy="data.project">
                                                            </div>
                                                            <x-maz-input-error for="data.project" />
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4">
                                                        <div class="col-12 col-md-12">
                                                            <label class="fw-normal">Message</label>
                                                            <div class="text-muted">
                                                                <textarea class="form-control"wire:model.lazy="data.message"></textarea>
                                                            </div>
                                                            <x-maz-input-error for="data.message" />
                                                        </div>
                                                    </div>
                                                    <div class="row text-right">
                                                        <div class="col-12 col-md-12">
                                                            <button class="btn btn-tertiary">Submit</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <x-slot name="script">
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.6.7/dist/sweetalert2.all.min.js"></script>
        <script>
            document.addEventListener("DOMContentLoaded", ev => {
                // Toast Alert notification
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                })

                // To execute alert via livewire
                window.livewire.on('alert', (content = '', status = 'success') => {
                    // Status warning, error, success, info
                    Toast.fire({
                        icon: status,
                        title: content,
                    })
                })
            })
        </script>
    </x-slot>
</div>
