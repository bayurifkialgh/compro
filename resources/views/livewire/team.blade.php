<div>
    <x-slot name="title">
        {{ $title }}
    </x-slot>
    <div class="section py-0">
        <div class="container mt-n10 mt-lg-n12 z-2 mb-5">
            <div class="row justify-content-center">
                <div class="col-12 col-md-12 mt-5">
                    <div class="card shadow mb-6">
                        <div class="card-body px-5 py-5 text-center text-md-left">
                            <div class="row align-items-center">
                                <div class="col-md-12">
                                    <h2 class="mb-3">Our Team</h2>
                                    <div class="row">
                                        @foreach($teams as $team)
                                            <div class="col-6 col-md-4">
                                                <div class="card border-0 overflow-hidden">
                                                    <div class="position-relative">
                                                        <img src="{{ url('storage/team/' . $team->image) }}" class="card-img-top" alt="{{ $team->name }}" loading="lazy">
                                                    </div>
                                                    <div class="card-body position-relative mt-n6 mx-2 bg-white border border-gray-300 text-center rounded">
                                                        <h3 class="h5 card-title">
                                                            {{ $team->name }}
                                                        </h3>
                                                        <p class="mt-3">
                                                            {{ $team->description }}
                                                        </p>
                                                        <div class="row">
                                                            @php
                                                                $link_social = [
                                                                    'facebook' => 'https://facebook.com/',
                                                                    'twitter' => 'https://twitter.com/',
                                                                    'instagram' => 'https://instagram.com/',
                                                                    'linkedin' => 'https://linkedin.com/',
                                                                    'youtube' => 'https://youtube.com/',
                                                                    'github' => 'https://github.com/',
                                                                    'discord' => 'https://discord.com/users/',
                                                                ];

                                                                $socials = json_decode($team->socials, true);
                                                            @endphp
                                                            @if($socials)
                                                                @foreach($socials as $k => $v)
                                                                    <div class="col-4 col-md-4">
                                                                        <a href="{{ $link_social[$k] . $v }}">
                                                                            <span class="me-1">
                                                                                <span class="fab fa-{{ $k }}"></span>
                                                                            </span>
                                                                        </a>
                                                                    </div>
                                                                @endforeach
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

