<div>
    <x-slot name="title">
        {{ $title }}
    </x-slot>

    <div class="section py-0">
        <div class="container mt-n10 mt-lg-n12 z-2 mb-5">
            <div class="row justify-content-center">
                <div class="col-12 col-md-10 mt-5">
                    <div class="card shadow mb-6">
                        <div class="card-body px-5 py-5 text-center text-md-left">
                            <div class="row align-items-center">
                                <div class="col-md-12">
                                    <h2 class="mb-3">About Us</h2>
                                    <p class="mb-0">
                                        {{ $aboutUs['description'] }}
                                    </p>
                                    <a href="{{ url('/about-us') }}"
                                        class="btn btn-primary mt-2 animate-up-2">
                                        <span class="fas fa-info-circle me-2"></span> Detail
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row justify-content-center mb-5 mb-lg-6">
                @foreach($services as $srv)
                    <div class="col-6 col-md-3 text-center mb-4">
                        <div class="icon icon-shape icon-lg bg-white shadow-lg border-light rounded-circle mb-4">
                            <span class="{{ $srv->icon }} text-tertiary"></span>
                        </div>
                        <h5 class="fw-bolder">{{ $srv->name }}</h5>
                        <p class="text-gray"></p>
                    </div>
                @endforeach
            </div>

            <div class="row justify-content-between align-items-center mb-5 mb-lg-7">
                <div class="col-12 col-lg-5 mb-5 mb-lg-0 order-lg-2">
                    <h2 class="h1">
                        Tech Expertise
                    </h2>
                    <p class="mb-4 lead fw-bold">Technology we used on a daily basis</p>
                    <ul class="mb-4">
                        @foreach($techExpertises as $te)
                            <li>
                                {{ $te->name }}
                                <span class="{{ $te->icon }} text-tertiary"></span>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-12 col-lg-6 order-lg-1">
                    <img src="{{ url('fe') }}/assets/img/pages/code.svg" alt="Front pages overview">
                </div>
            </div>
            <div class="row justify-content-between align-items-center mb-5 mb-lg-7">
                <div class="col-lg-5 mb-5 mb-lg-0">
                    <h2 class="h1 d-flex align-items-center">
                        Portofolio
                    </h2>
                    <p class="mb-4 lead fw-bold">Our Portfolio</p>
                    <p class="mb-4">Professional Stuffs Ready to Help Your Business</p>
                    <a href="{{ url('/portfolio') }}" class="btn btn-primary mt-2 animate-up-2">
                        <span class="fas fa-info-circle me-2"></span> Detail
                    </a>
                </div>
                <div class="col-lg-6">
                    <img src="{{ url('') }}/portfolio.svg" alt="MapBox Leaflet.js Custom Integration Mockup">
                </div>
            </div>
        </div>
    </div>


    <section class="section section-lg bg-secondary mt-4">
        <div class="container">
            <div class="row justify-content-center text-center text-white">
                <div class="col-lg-10 col-xl-12">
                    <h2 class="fw-light mb-4">OUR <span class="fw-bold"> CLIENT</span></h2>
                    <p class="lead">
                        Trusted by Leading Business
                    </p>
                </div>
            </div>
            <div class="row mt-4 mt-lg-6">
                @foreach($clients as $cl)
                    <div class="col-md-6 col-sm-6 col-lg-3">
                        <div class="card shadow-soft border-soft bg-gray-200 p-5 text-center mb-4">
                            <img src="{{ url('storage/client/' . $cl->image) }}" class="color-shape rounded m-auto">
                            <div class="mt-4">
                                <h2 class="h5 text-dark">{{ $cl->name }}</h2>
                                {{-- <span class="fw-bold text-primary">{{ $cl->name }}</span> --}}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
</div>

