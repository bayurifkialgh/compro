@livewireScripts
<script src="{{ mix('js/app.js') }}"></script>
<script src="{{ asset('/vendors/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
<script src="{{ asset('/vendors/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('/js/bootstrap.bundle.min.js') }}"></script>

<script src="{{ asset('/js/main.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.6.7/dist/sweetalert2.all.min.js"></script>
<script>
    document.addEventListener("DOMContentLoaded", ev => {
        // Toast Alert notification
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
        })

        // To execute alert via livewire
        window.livewire.on('alert', (content = '', status = 'success') => {
            // Status warning, error, success, info
            Toast.fire({
                icon: status,
                title: content,
            })
        })

        // Confirm
        window.livewire.on('confirm', async (id, type = 'delete', title = 'Are you sure?', icon = 'warning', content = '') => {
            const confirm = await Swal.fire({
                title: title,
                icon: icon,
                confirmButtonText: 'Ok',
                showCancelButton: true,
            })

            // If true
            if (confirm.isConfirmed) {
                window.livewire.emit(type, id)
            }
        })

        // Close modal bootstrap 5
        window.livewire.on('closeModal', (id = 'modal-crud') => {
            const myModal = bootstrap.Modal.getInstance(document.getElementById(id))
            // const modalBackdrop = document.getElementsByClassName('modal-backdrop')[0]
            // modalBackdrop.classList.remove('modal-backdrop')
            myModal.hide()
        })

        // Scroll up
        window.livewire.on('scrollUp', () => {
            window.scrollTo(0, 0)
        })
    })
</script>

{{ $script ?? ''}}

