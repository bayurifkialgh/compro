<x-maz-sidebar :href="route('admin.dashboard')" :logo="asset('images/logo/logo.png')">

    <!-- Add Sidebar Menu Items Here -->

    <x-maz-sidebar-item name="Dashboard" :link="route('admin.dashboard')" icon="bi bi-grid-fill"></x-maz-sidebar-item>
    <x-maz-sidebar-item name="About Us" :link="route('admin.about-us')" icon="bi bi-person-square"></x-maz-sidebar-item>
    <x-maz-sidebar-item name="Tech Expertises" :link="route('admin.tech-expertises')" icon="bi bi bi-code"></x-maz-sidebar-item>
    <x-maz-sidebar-item name="Services" :link="route('admin.services')" icon="bi bi-bag-fill"></x-maz-sidebar-item>
    <x-maz-sidebar-item name="Clients" :link="route('admin.clients')" icon="bi bi-file-earmark-person-fill"></x-maz-sidebar-item>
    <x-maz-sidebar-item name="Portfolios" :link="route('admin.portfolios')" icon="bi bi-file-zip"></x-maz-sidebar-item>
    <x-maz-sidebar-item name="Teams" :link="route('admin.teams')" icon="bi bi-emoji-sunglasses"></x-maz-sidebar-item>
    <x-maz-sidebar-item name="Contact" :link="route('admin.contact')" icon="bi bi-headset"></x-maz-sidebar-item>

    <x-maz-sidebar-item name="Configuration" icon="bi bi-file-plus-fill">
        <x-maz-sidebar-sub-item name="Webiste" :link="route('admin.configuration.website')"></x-maz-sidebar-sub-item>
        <x-maz-sidebar-sub-item name="Meta Data" :link="route('admin.configuration.meta')"></x-maz-sidebar-sub-item>
        <x-maz-sidebar-sub-item name="Socials" :link="route('admin.configuration.social')"></x-maz-sidebar-sub-item>
        <x-maz-sidebar-sub-item name="Privacy Policy" :link="route('admin.configuration.privacy-policy')"></x-maz-sidebar-sub-item>
        <x-maz-sidebar-sub-item name="Term & Condition" :link="route('admin.configuration.term-cond')"></x-maz-sidebar-sub-item>
    </x-maz-sidebar-item>

</x-maz-sidebar>
