<!--

=========================================================
* Pixel Free Bootstrap 5 UI Kit
=========================================================

* Product Page: https://themesberg.com/product/ui-kit/pixel-free-bootstrap-5-ui-kit
* Copyright 2021 Themesberg (https://www.themesberg.com)

* Coded by https://themesberg.com

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. Contact us if you want to remove it.

-->

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Primary Meta Tags -->
    <title>{{ $websites['app_name'] }} | {{ $title ?? '' }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    @foreach ($metas as $key => $val)
        <meta name="{{ $key }}" content="{{ $val }}">
    @endforeach

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="120x120" href="{{ url('storage/websites/' . $websites['app_apple_icon']) }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ url('storage/websites/' . $websites['icon_32x32']) }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('storage/websites/' . $websites['icon_16x16']) }}">
    <link rel="manifest" href="{{ url('storage/websites/' . $websites['manifest']) }}">
    <link rel="mask-icon" href="{{ url('storage/websites/' . $websites['app_icon']) }}">

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

    @include('layouts.partials-main.styles')

</head>

<body>

    @include('layouts.partials-main.header')
    <main>

        {{-- <div class="preloader bg-dark flex-column justify-content-center align-items-center">
            <svg id="loader-logo" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 64 78.4">
                <path fill="#fff" d="M10,0h1.2V11.2H0V10A10,10,0,0,1,10,0Z" />
                <rect fill="none" stroke="#fff" stroke-width="11.2" x="40" y="17.6" width="0" height="25.6" />
                <rect fill="none" stroke="#fff" stroke-opacity="0.4" stroke-width="11.2" x="23" y="35.2" width="0" height="25.6" />
                <path fill="#fff" d="M52.8,35.2H64V53.8a7,7,0,0,1-7,7H52.8V35.2Z" />
                <rect fill="none" stroke="#fff" stroke-width="11.2" x="6" y="52.8" width="0" height="25.6" />
                <path fill="#fff" d="M52.8,0H57a7,7,0,0,1,7,7h0v4.2H52.8V0Z" />
                <rect fill="none" stroke="#fff" stroke-opacity="0.4" stroke-width="11.2" x="57.8" y="17.6" width="0" height="11.2" />
                <rect fill="none" stroke="#fff" stroke-width="11.2" x="6" y="35.2" width="0" height="11.2" />
                <rect fill="none" stroke="#fff" stroke-width="11.2" x="40.2" y="49.6" width="0" height="11.2" />
                <path fill="#fff" d="M17.6,67.2H28.8v1.2a10,10,0,0,1-10,10H17.6V67.2Z" />
                <rect fill="none" stroke="#fff" stroke-opacity="0.4" stroke-width="28.8" x="31.6" width="0" height="11.2" />
                <rect fill="none" stroke="#fff" x="14" stroke-width="28.8" y="17.6" width="0" height="11.2" />
            </svg>
        </div> --}}

        <!-- Hero -->
        <section class="section-header overflow-hidden pt-7 pt-lg-8 pb-9 pb-lg-12 bg-primary text-white">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <div class="bootstrap-big-icon d-none d-lg-block">
                            <img src="{{ url('/storage/about-us/' . $aboutUs->logo) }}" class="d-block my-1" width="100%">
                        </div>
                        <h1 class="fw-bolder display-2">{{ $aboutUs['name'] }}</h1>
                        <h2 class="lead fw-normal text-muted mb-4 px-lg-10">{{ $aboutUs['slogan'] }}</h2>
                        <!-- Button Modal -->
                        @if($routeName != 'contact-us')
                            <div class="d-flex justify-content-center align-items-center mb-5">
                                <a href="{{ url('/contact-us') }}" class="btn btn-tertiary mb-3 mt-2 me-2 me-md-3 animate-up-2">
                                    <span class="d-none d-md-inline me-2">Contact Us</span>
                                    <span class="fas fa-phone"></span>
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <figure class="position-absolute bottom-0 left-0 w-100 d-none d-md-block mb-n2">
                <svg class="fill-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 3000 185.4">
                    <path d="M3000,0v185.4H0V0c496.4,115.6,996.4,173.4,1500,173.4S2503.6,115.6,3000,0z"></path>
                </svg>
            </figure>
        </section>

        {{ $slot }}

    </main>
    @include('layouts.partials-main.footer')
    @include('layouts.partials-main.scripts')
</body>
</html>
