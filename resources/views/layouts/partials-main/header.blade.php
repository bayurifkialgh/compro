<header class="header-global">
    <nav id="navbar-main" aria-label="Primary navigation"
        class="navbar navbar-main navbar-expand-lg navbar-theme-primary headroom navbar-dark">
        <div class="container position-relative">
            <a class="navbar-brand me-lg-5" href="{{ url('/') }}">
                {{-- /assets/img/brand/light.svg --}}
                {{-- /assets/img/brand/dark.svg --}}
                {{-- /assets/img/brand/dark.svg --}}
                <img class="navbar-brand-dark" src="{{ url('/storage/about-us/' . $aboutUs->logo) }}" alt="{{ $aboutUs->name }} logo light">
                <img class="navbar-brand-light" src="{{ url('/storage/about-us/' . $aboutUs->logo) }}" alt="{{ $aboutUs->name }} logo dark">
            </a>
            <div class="navbar-collapse collapse me-auto" id="navbar_global">
                <div class="navbar-collapse-header">
                    <div class="row">
                        <div class="col-6 collapse-brand">
                            <a href="{{ url('/') }}">
                                <img src="{{ url('/storage/about-us/' . $aboutUs->logo) }}" alt="{{ $aboutUs->name }} logo">
                            </a>
                        </div>
                        <div class="col-6 collapse-close">
                            <a href="#navbar_global" class="fas fa-times" data-bs-toggle="collapse"
                                data-bs-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false"
                                title="close" aria-label="Toggle navigation"></a>
                        </div>
                    </div>
                </div>
                <ul class="navbar-nav navbar-nav-hover align-items-lg-center">
                    <li class="nav-item">
                        <a href="{{ url('/') }}" class="nav-link">
                            Home
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown"
                            id="services-dropdown" aria-expanded="false">
                            Services
                            <span class="fas fa-angle-down nav-link-arrow ms-1"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-lg" aria-labelledby="services-dropdown">
                            <div class="col-auto px-0">
                                <div class="list-group list-group-flush">
                                    @foreach($services as $srv)
                                        <a href="{{ url('service/' . $srv->slug) }}" class="list-group-item list-group-item-action d-flex align-items-center p-0 py-3 px-lg-4">
                                            <span class="icon icon-sm"><span class="{{ $srv->name }}"></span></span>
                                            <div class="ms-4">
                                                <span class="d-block font-small fw-bold mb-0">
                                                    {{ $srv->name }}
                                                </span>
                                            </div>
                                        </a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown"
                            id="profile-dropdown" aria-expanded="false">
                            Profile
                            <span class="fas fa-angle-down nav-link-arrow ms-1"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-lg" aria-labelledby="profile-dropdown">
                            <div class="col-auto px-0">
                                <div class="list-group list-group-flush">
                                    <a href="{{ url('about-us') }}" class="list-group-item list-group-item-action d-flex align-items-center p-0 py-3 px-lg-4">
                                        <div class="ms-4">
                                            <span class="d-block font-small fw-bold mb-0">
                                                About us
                                            </span>
                                        </div>
                                    </a>
                                    <a href="{{ url('team') }}" class="list-group-item list-group-item-action d-flex align-items-center p-0 py-3 px-lg-4">
                                        <div class="ms-4">
                                            <span class="d-block font-small fw-bold mb-0">
                                                Team
                                            </span>
                                        </div>
                                    </a>
                                    <a href="{{ url('portfolio') }}" class="list-group-item list-group-item-action d-flex align-items-center p-0 py-3 px-lg-4">
                                        <div class="ms-4">
                                            <span class="d-block font-small fw-bold mb-0">
                                                Portfolio
                                            </span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('/contact-us') }}" class="nav-link">
                            Contact Us
                        </a>
                    </li>
                </ul>
            </div>
            <div class="d-flex align-items-center">
                <button class="navbar-toggler ms-2" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </div>
    </nav>
</header>
