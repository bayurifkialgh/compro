<!-- Core -->
<script src="{{ url('fe') }}/vendor/@popperjs/core/dist/umd/popper.min.js"></script>
<script src="{{ url('fe') }}/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="{{ url('fe') }}/vendor/headroom.js/dist/headroom.min.js"></script>

<!-- Vendor JS -->
<script src="{{ url('fe') }}/vendor/onscreen/dist/on-screen.umd.min.js"></script>
<script src="{{ url('fe') }}/vendor/jarallax/dist/jarallax.min.js"></script>
<script src="{{ url('fe') }}/vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js"></script>
<script src="{{ url('fe') }}/vendor/vivus/dist/vivus.min.js"></script>
{{-- <script src="{{ url('fe') }}/vendor/vanillajs-datepicker/dist/js/datepicker.min.js"></script> --}}

<script async defer src="https://buttons.github.io/buttons.js"></script>

@livewireScripts

<!-- Pixel JS -->
<script src="{{ url('fe') }}/assets/js/pixel.js"></script>


{{ $script ?? ''}}
