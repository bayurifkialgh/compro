<footer class="footer pt-6 pb-5 bg-primary text-white">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <img class="navbar-brand-dark mb-4" height="35" src="{{ url('/storage/about-us/' . $aboutUs->logo) }}" alt="{{ $aboutUs->name }} logo light">
                <h4>{{ $aboutUs->name }}</h4>
                <p>{{ $aboutUs->slogan }}</p>
                <p>{{ $aboutUs->address }}</p>
                <ul class="social-buttons mb-5 mb-lg-0">
                    <li>
                        <a href="{{ $socials['instagram'] }}" aria-label="instagram social link"
                            class="icon-white me-2">
                            <span class="fab fa-instagram"></span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ $socials['twitter'] }}" aria-label="twitter social link"
                            class="icon-white me-2">
                            <span class="fab fa-twitter"></span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ $socials['facebook'] }}" class="icon-white me-2"
                            aria-label="facebook social link">
                            <span class="fab fa-facebook"></span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ $socials['github'] }}" aria-label="github social link"
                            class="icon-white me-2">
                            <span class="fab fa-github"></span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ $socials['linkedin'] }}" class="icon-white"
                            aria-label="linkedin social link">
                            <span class="fab fa-linkedin"></span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-6 col-md-4 mb-5 mb-lg-0">
                <span class="h5">Services</span>
                <ul class="footer-links mt-2">
                    @foreach($services as $srv)
                        <li><a target="_blank" href="{{ url('service/' . $srv->slug) }}">{{ $srv->name }}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="col-6 col-md-4 mb-5 mb-lg-0">
                <span class="h5">Pages</span>
                <ul class="footer-links mt-2">
                    <li>
                        <a target="_blank" href="{{ url('term-condition') }}">Term And Condition</a>
                        <a target="_blank" href="{{ url('privacy-policy') }}">Privacy Policy</a>
                    </li>
                </ul>
            </div>
        </div>
        <hr class="bg-secondary my-3 my-lg-5">
        <div class="row">
            <div class="col mb-md-0">
                <div class="d-flex text-center justify-content-center align-items-center" role="contentinfo">
                    <p class="fw-normal font-small mb-0">Copyright © {{ $aboutUs->name }} 2020-<span
                            class="current-year">{{ date('Y') }}</span>. All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>
