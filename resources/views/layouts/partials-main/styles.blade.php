<!-- Fontawesome -->
<link type="text/css" href="{{ url('fe') }}/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">

<!-- Pixel CSS -->
<link type="text/css" href="{{ url('fe') }}/css/pixel.css" rel="stylesheet">

@livewireStyles

{{ $style ?? '' }}
