<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }} | {{ $title ?? '' }}</title>

        <!-- Styles -->
        @include('layouts.partials-admin.styles')
    </head>
    <body>
        <div id="app">
            @include('layouts.partials-admin.sidebar')

            <div id="main" class='layout-navbar'>
                @include('layouts.partials-admin.header')
                <div id="main-content">

                    <div class="page-title">
                        {{ $header ?? '' }}
                    </div>

                    <div class="page-heading">
                        {{ $slot }}
                    </div>

                    @include('layouts.partials-admin.footer')
                </div>
            </div>
        </div>

        <!-- Scripts -->
        @include('layouts.partials-admin.scripts')

    </body>
</html>
