<div class="card-header">
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
            <select wire:model.lazy="paginate" class="form-control sm">
                <option value="5">5 entries per page</option>
                <option value="10">10 entries per page</option>
                <option value="50">50 entries per page</option>
                <option value="100">100 entries per page</option>
            </select>
        </div>
        <div class="col-lg-4 col-md-4"></div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="ti ti-search"></i>
                </span>
                <input wire:model="search" type="text" class="form-control sm"
                    placeholder="Search......" autocomplete="off">
            </div>
        </div>
        <div class="col-lg-12 col-md-12">
            <button class="btn btn-success float-end mt-3" wire:click="clearForm" data-bs-toggle="modal"
            data-bs-target="#modal-crud">
                <i class="bi bi-plus"></i>
                Create
            </button>
        </div>
    </div>
</div>
