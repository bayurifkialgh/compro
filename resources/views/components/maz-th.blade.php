@props(['orderby', 'order', 'field'])

<th x-data x-on:click="$wire.changeOrder('{{$field}}')" style="cursor: pointer">
    @if($orderby == $field)
        @if($order == 'asc')
            <i class="bi bi-caret-up-fill"></i>
        @else
            <i class="bi bi-caret-down-fill"></i>
        @endif
    @endif
    {{ $slot->isEmpty() ? 'th' : $slot }}
</th>
